COMPILERFLAGS=--std=c17 -Wall -pedantic -Isrc/ -ggdb -Wextra -DDEBUG
BUILDDIR=build
SRCDIR=src
COMPILER=gcc

all: $(BUILDDIR)/mem.o $(BUILDDIR)/util.o $(BUILDDIR)/mem_debug.o $(BUILDDIR)/main.o $(BUILDDIR)/test.o
	$(COMPILER) -o $(BUILDDIR)/main $^

build:
	mkdir -p $(BUILDDIR)

$(BUILDDIR)/test.o: $(SRCDIR)/test.c build
	$(COMPILER) -c $(COMPILERFLAGS) $< -o $@

$(BUILDDIR)/main.o: $(SRCDIR)/main.c build
	$(COMPILER) -c $(COMPILERFLAGS) $< -o $@

$(BUILDDIR)/mem.o: $(SRCDIR)/mem.c build
	$(COMPILER) -c $(COMPILERFLAGS) $< -o $@

$(BUILDDIR)/mem_debug.o: $(SRCDIR)/mem_debug.c build
	$(COMPILER) -c $(COMPILERFLAGS) $< -o $@

$(BUILDDIR)/util.o: $(SRCDIR)/util.c build
	$(COMPILER) -c $(COMPILERFLAGS) $< -o $@

clean:
	rm -rf $(BUILDDIR)
